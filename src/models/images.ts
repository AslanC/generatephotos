import {Schema, model, Document} from 'mongoose';

const schema = new Schema ({
    name: String,
    address: String,
    experience: String
});

export interface IImage extends Document {
    name: string,
    address: string,
    experience: string
}

export default model<IImage>('image', schema);
