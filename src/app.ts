import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import path from 'path';

import router from './routes/index'

const app = express();

app.set('port', process.env.Port || 8020);

app.use(cors());

//middlewares
app.use(morgan('dev'));

app.use(express.json());

//routes
app.use('/api', router);

//folder to store public files
app.use('/upload', express.static(path.resolve('upload')));

export default app;