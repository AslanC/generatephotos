import app from './app'
import dotenv from 'dotenv';
import {connection} from './database';

//init config
dotenv.config();



async function main() {
    connection();
    app.listen(app.get('port'), () => {
        console.log(`server started at http://localhost:${ app.get('port') }` );
    })
}

main();