import {Router} from 'express';

import {getImages, createImage, deleteImage, updateImage} from '../controllers/image.controller'
import multer from '../libs/multer'

const router = Router();

router.route('/')
    .get(getImages)
    .post(multer.array('images',20),createImage)
    .delete(deleteImage)
    .put(multer.single('images'),updateImage)

export default router;