import {Request, Response} from 'express';
import {createDocument, saveDocument, findDocuments, deleteDocument, findOneDocument, updateDocument} from '../libs/document';

export function getImages(req: Request, res: Response): Promise<Response> {
    const rs = req.headers.experience !== undefined ? req.headers.experience.toString() : '';

    return new Promise(async(resolve, reject) => {
        await findDocuments(rs).then(document => {
            res.json(document);
        }).catch(err => {
            res.send(err)
        })
    });
}

export async function createImage(req: Request, res: Response): Promise<Response> {
    let name = req.body.title;
    console.log(name);
    let experience = req.body.experience;
    
    console.log('saving image');

    const fs: Express.Multer.File[] = req.files as [];
    const response = await funResponse(fs, name, experience);

    return res.send(response);

}

export async function deleteImage(req: Request, res: Response): Promise<Response> {
    try {
        console.log(req.body.id);
        const ids:string = req.body.id;

        const response = await deleteDocument(ids);
    
        return res.send(response);   
    } catch (error) {
        console.log(error);
        return res.send(error);
    }
}

export async function updateImage(req: Request, res: Response): Promise<Response> {
    try {
        const rs = req.headers.id !== undefined ? req.headers.id.toString() : '';
        const doc = await findOneDocument(rs);
        
        if (!doc) {
            return res.send({ message: 'al parecer el archivo no existe' });
        }
        const fs: Express.Multer.File = req.file;
        const img = {
            name: fs.filename,
            address: fs.path,
            experience: req.headers.experience !== undefined ? req.headers.experience.toString() : ''
        }
        
        const data = await updateDocument(rs, img).then((result) => {
            return result;
        }).catch((err) => { return err });
        return res.send(data);
    } catch (error) {
        console.log(error);
        return res.send(error);
    }
}

async function funResponse(fs: Express.Multer.File[], name:string , experience:string): Promise<[{}]>{
    
    let response: [{}] = [{}];

    return new Promise(async(resolve, reject) => {
        for (let i = 0; i < fs.length; i++) {
            const element = fs[i];

            const document = await createDocument(element,name,experience);

            response.length === 1 ? response[0] = await saveDocument(document): response.push(await saveDocument(document));
        }

        resolve(response)
    });
}