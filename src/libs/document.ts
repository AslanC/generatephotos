import Image from '../models/images';
import path from 'path';
import fs from 'fs-extra';
import {IImage} from '../models/images'

export async function createDocument(document: any, name: string, description: string): Promise<{}> {
    
    return new Promise<{}>((resolve, reject) =>{
        const newImage = new Image({
            name: name,
            experience: description.toUpperCase(),
            address: document.path
        });

        resolve(newImage);
    });
}

export async function saveDocument(newFile:{}): Promise<{}> {
    const img = new Image(newFile);
    let json: {};
    try {
        await img.save();
        json = ({
            message: 'la ruta del archivo se guardó con exito en la base de datos',
            error: '',
            img
        });
    } catch (err) {
        console.log(err);
        json = ({
            message: '',
            error: 'Hubo un error al guardar',
        });
    }
    return json;
}

export async function findDocuments(description:string): Promise<IImage[]> {
    const qry = {"experience" : {$regex: '.*' + description.toUpperCase() + '.*'}};
    return new Promise(async (resolve, reject) => {              
        await Image.find(qry, (err, model) => {
            if(err) reject(err);
            resolve(model);
        });
    });
}

export function deleteDocument(ids: string): Promise<{}>{
    return new Promise(async(resolve, reject) => {
        try {
            const img = await Image.findByIdAndRemove(ids);
            if (img) {
                fs.unlink(path.resolve(img.address))
                    .then(() => {
                        resolve({
                            succes: true,
                            message: 'se eliminó con éxito',
                            image: img
                        });
                    }).catch((err) => {
                        reject({
                            succes: false,
                            message: err
                        })
                    })
            }
        } catch (error) {
            reject({
                succes: false,
                message: error
            })
        }
    })
}

export function updateDocument(id:string, img:{}): Promise<{}> {
    const filter = {_id: id};
    console.log(filter);
    return new Promise(async(resolve, reject) => {
        try {
            await Image.findByIdAndUpdate(filter,img,
            {new:true}).then((result) => {
                
                console.log(result);
                resolve({
                    succes: true,
                    message: result
                })
            }).catch(err => {
                console.log(err);
                reject({
                    succes: false,
                    message: err
                })
            })
        } catch (error) {
            console.log(error);
            reject({
                succes: false,
                message: error
            })
        }
    })
}

export function findOneDocument(id:string) : Promise<Boolean> {
    let res = false;

    return new Promise( async(resolve, reject) => {
        try {
           await Image.findById(id, err => {
                if(err) {reject(false)}
                resolve(true);
            })
        } catch (error) {
            reject(false);
        }
    })
    
}