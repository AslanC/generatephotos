import multer from 'multer';
import path from 'path';

//config
const storage = multer.diskStorage({
    destination: 'upload',
    filename: (req, file, cb) => {
        cb(null, req.body.title + '-' + Date.now() + path.extname(file.originalname));
    }
})

export default multer({storage:storage});